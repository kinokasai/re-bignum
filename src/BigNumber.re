type t;

[@bs.module] [@bs.new]
external bigNumber : string => t = "bignumber.js"

[@bs.module] [@bs.new]
external intBigNumber : int => t = "bignumber.js"

[@bs.send]
external to_string: t => string = "toString";

[@bs.send]
external is_equal_to: t => t => bool = "isEqualTo";

/* Operators */

[@bs.send]
external plus_bignum: t => t => t = "plus";

[@bs.send]
external plus_float: t => float => t = "plus";

[@bs.send]
external plus_int: t => int => t = "plus";

[@bs.send]
external multiplied_by: t => t => t = "multipliedBy";

[@bs.send]
external multiplied_by_int: t => int => t = "multipliedBy";

[@bs.send]
external divided_by: t => t => t = "dividedBy";

[@bs.send]
external divided_by_int: t => int => t = "dividedBy";

[@bs.send]
external divided_to_int_by: t => t => int = "dividedToIntegerBy";

/* Modifiers */

[@bs.send]
external decimal_places: t => int => t = "decimalPlaces";

let make = str => bigNumber(str)

let of_int = intBigNumber

let equals = is_equal_to
